import dotenv from 'dotenv'
import express from 'express';
import router from "./routes.js";
import { sendEmail } from './send-email.js';

dotenv.config()

const app = express();
app.use('/', router);

app.listen(process.env.PORT, () => {
  console.log(`App listening at http://localhost:${process.env.PORT}`);
  sendEmail(process.env.MAIL, 'GeoTwitter');
});
