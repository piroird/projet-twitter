
export class Position {
  private tweet_id: string;
  private country_code: string;
  private longitude: number;
  private latitude: number;

  constructor(tweet_id: string, country_code: string, longitude: number, latitude: number){
    this.tweet_id = tweet_id;
    this.country_code = country_code;
    this.longitude = longitude;
    this.latitude = latitude;
  }

  public getCountryCode(): string {
    return this.country_code;
  }

  public getLongitude(): number {
    return this.longitude;
  }

  public getLatitude(): number {
    return this.latitude;
  }

  public getTweetId(): string {
    return this.tweet_id;
  }
}
