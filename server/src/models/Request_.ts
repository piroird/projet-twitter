// log data of each request

export class Request_ {
  private rid: string;
  private query: string;
  private server: "Twitter" | "LocalServer";
  private nb_rqTwitter: number; // number of request to Twitter API
  private called_at: string; // string, due to redis types

  constructor(query: string, server: "Twitter" | "LocalServer", nb_rqTwitter: number, rid?: string, called_at?: string) {
    if ((typeof rid !== 'undefined') && (typeof called_at !== 'undefined')) {
      // request from cache
      this.rid = rid;
      this.called_at = called_at;
    }
    else {
      // new Request
      const now: Date = new Date();
      this.rid = Date.parse(now.toString()) + (Math.random() + 1).toString(36).substring(5);
      this.called_at = now.toLocaleString("fr-FR");
    }
    this.server = server;
    this.query = query;
    this.nb_rqTwitter = nb_rqTwitter;
  }

  public getRid(): string {
    return this.rid;
  }

  public getQuery(): string {
    return this.query;
  }

  public getServer(): string {
    return this.server;
  }

  public getNbRqTwitter(): number {
    return this.nb_rqTwitter;
  }

  public getCalledAt(): string {
    return this.called_at;
  }

}