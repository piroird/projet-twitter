import fetch from 'node-fetch';

const TWITTER_API_URL = "https://api.twitter.com/2/";


export function getSearchQuery(query: string, next_token?:string|null, maxSize=100){
  // https://developer.twitter.com/en/docs/twitter-api/tweets/search/api-reference/get-tweets-search-all#Optional

  let res = TWITTER_API_URL + "tweets/search/recent"
    + "?query=" + encodeURIComponent(query)
    + "&max_results=" + maxSize
    + "&expansions=geo.place_id"
    + "&place.fields=country_code,geo"
    + (next_token == null ? "" : "&next_token="+next_token);

  return res;
}

export function callTwitter(url: string, method="GET"): Promise<any>{
  return new Promise((resolve, reject) => {
    fetch(url, {
      method: method,
      headers: {
        'Authorization': `Bearer ${process.env.BEARER}`
      }
    }).then(res => {
      if(res.status == 200){
        resolve(res.json());
      }
      else {
        reject({status: res.status, statusText: res.statusText, url: res.url});
      }
    });
  });
}
