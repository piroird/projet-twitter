import { createClient } from "redis";
import { Position } from "./models/Position.js";
import { Request_ } from "./models/Request_.js";

const S_QUERY: string = "S_QUERY::"; // S_QUERY::query : a set of place_ids based on query
const H_POSITION: string = "H_POSITION::"; // H_POSITION::place_id : a hash set of position's info
const L_REQUEST: string = "L_REQUEST::"; // L_REQUEST::rid : a hash set of request's info
const LOCAL_REQUEST: string = "NB_LOCAL_REQUEST"; // number of request to local server
const REMOTE_REQUEST: string = "NB_REMOTE_REQUEST"; // number of request to Twitter server

const redis_cli = createClient(); // Redis client

async function checkClient() {
  if (!redis_cli.isOpen) {
    try {
      await redis_cli.connect();
    }
    catch (err) {
      return err;
    }
  }
}

export async function saveReceivedPositions(query: string, positions: Position[]) {

  try {
    checkClient();

    const s_query: string = S_QUERY + query;

    positions.forEach(async (pos: Position) => {
      // name of a hash set
      const key: string = H_POSITION + pos.getTweetId();

      //check existence of this hash set
      const existence: number = await redis_cli.exists(key);

      // add this position into cache
      if (existence !== 1) {
        await redis_cli.hSet(key, "country_code", pos.getCountryCode());
        await redis_cli.hSet(key, "longitude", pos.getLongitude());
        await redis_cli.hSet(key, "latitude", pos.getLatitude());
      }

      // update the list of place_ids
      await redis_cli.sAdd(s_query, pos.getTweetId());
    });
  }
  catch (err) {
    return Promise.reject({"error": err});
  }

}

export async function getSavedPositionsByQuery(query: string, max: number = 20): Promise<Position[]> {
  return new Promise(async (resolve, reject) => {

    try {
      await checkClient();

      const positions = [];

      const s_query: string = S_QUERY + query;

      // get all members of the set
      const place_ids: string[] = await redis_cli.sMembers(s_query);

      if (place_ids.length > 0) {

        const amount: number = Math.min(place_ids.length, max);

        for (let i: number = 0; i < amount; i++) {

          const key: string = H_POSITION + place_ids[i];
          const data = await redis_cli.hGetAll(key);

          positions.push(new Position(
            place_ids[i],
            data.country_code,
            Number(data.longitude),
            Number(data.latitude))
          );
        }
      }
      // log this request
      saveRequest(new Request_(query, "LocalServer", 0)).then().catch((e) => reject(e));

      resolve(positions);
    }

    catch (e) {
      reject({"error" : e});
    }
  });
}

// log the request into cache
export async function saveRequest (request_: Request_) {

  try {
    checkClient();

    const key: string = L_REQUEST + request_.getRid();

    await increase_rqNumber(request_.getServer() as "Twitter" | "LocalServer");
    await redis_cli.hSet(key, "query", request_.getQuery());
    await redis_cli.hSet(key, "server", request_.getServer());
    await redis_cli.hSet(key, "nb_rqTwitter", request_.getNbRqTwitter());
    await redis_cli.hSet(key, "called_at", request_.getCalledAt());

  }
  catch (err) {
    return Promise.reject({"error": err});
  }

}

// get all today logged requests
export async function getRequests(): Promise<Request_[]> {

  return new Promise(async (resolve, reject) => {

    try {
      await checkClient();

      const keys: string[] = await redis_cli.keys(L_REQUEST + "*");
      const requests: Request_[] = [];

      if (keys.length > 0) {

        for (let i: number = 0; i < keys.length; i++) {
          const data = await redis_cli.hGetAll(keys[i]);
          requests.push(new Request_(
            data.query,
            data.server as "Twitter" | "LocalServer",
            Number(data.nb_rqTwitter),
            keys[i].substring(L_REQUEST.length),
            data.called_at
          ));

        }
      }
      resolve(requests);
    }

    catch (e) {
      reject({"error" : e});
    }
  });
}

// delete all the logs
export async function dailyLogReset() {
  try {
    checkClient();

    // get all keys to delete
    const keys: string[] = await redis_cli.keys(L_REQUEST + "*");
    if (keys.length > 0) {
      await redis_cli.del(keys);
    }

    // reset request number
    await redis_cli.del(REMOTE_REQUEST);
    await redis_cli.del(LOCAL_REQUEST);
  }
  catch (err) {
    return Promise.reject({"error": err});
  }
}

// get the number of requests logged
export async function getNumberOfRequest(type: "Twitter" | "LocalServer" | "Total"): Promise<number> {
  return new Promise(async (resolve, reject) => {
    try {
      await checkClient();

      let nb: number = 0;
      if (type === "Twitter") {
        nb = Number(await redis_cli.get(REMOTE_REQUEST));
      }
      else if ( type === "LocalServer") {
        nb = Number(await redis_cli.get(LOCAL_REQUEST));
      }
      else {
        const nb_local: number = Number(await redis_cli.get(LOCAL_REQUEST));
        const nb_remote: number = Number(await redis_cli.get(REMOTE_REQUEST));
        nb = nb_local + nb_remote;
      }
      resolve(nb);
    }
    catch (e) {
      reject({"error" : e});
    }
  });
}

// increase the number of requests
async function increase_rqNumber(server: "Twitter" | "LocalServer") {
  try {
    await checkClient();
    if (server === "Twitter") {
      await redis_cli.incr(REMOTE_REQUEST);
    }
    else {
      await redis_cli.incr(LOCAL_REQUEST);
    }
  }
  catch (err) {
    return Promise.reject({"error": err});
  }
}
