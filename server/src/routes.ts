import express from 'express';
import * as functions from "./functions.js";
import { saveReceivedPositions, getSavedPositionsByQuery } from './redis.js';

const router = express.Router();


router.get('/getLocations/', async (req, res) => {
  let query: string = req.query.query as string;
  let minimum: number|undefined = ("minimum" in req.query ? Number(req.query.minimum) : undefined);
  let limit: number|undefined = ("limit" in req.query ? Number(req.query.limit) : undefined);

  if (query == undefined) {
    res.status(500);
    res.send("Parameter 'query' is mandatory.");
  }
  else {
    functions.getLocations(query as string, limit, minimum)
    .then(positions => {
      res.send(positions);
      saveReceivedPositions(query, positions).then().catch(e => console.log(e));
    })
    .catch(err => {
      res.status(500);
      res.send(err);
    });
  }
});

router.get('/getLocalLocations/', async (req, res) => {
  const query: string = req.query.query as string;
  const max_result: number = Number(req.query.max_result) || 20;

  if (query == undefined) {
    res.status(500);
    res.send("Parameter 'query' is mandatory.");
  }
  else {
    getSavedPositionsByQuery(query, max_result)
    .then(positions => {
      if (positions.length === 0) {
        return res.send({"message": "No local data for this query."});
      }
      res.send(positions);
    })
    .catch(err => {
      res.status(500);
      res.send(err);
    });
  }
});

export default router;
