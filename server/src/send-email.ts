import nodemailer from 'nodemailer';
import cron from 'node-cron';
import { getRequests, dailyLogReset } from './redis.js';

const transport = nodemailer.createTransport({
	service: 'Gmail',
        auth: {
            user: 'projettwitter22@gmail.com',
            pass: '#projet22',
        },
    });

export function sendEmail(to: any, subject: any) {
    let message: string = "";

    cron.schedule('*/5 * * * *', () => {
        getRequests()
	.then(requests => {
		if (requests.length === 0) {
		    message = "Aucune nouvelle requête";
		}
		else {
		    if (requests.length == 1){
			message = "<b>" + 1 + " nouvelle requête effectuée :</b><br>";
		    }
		    else {
			message = "<b>" + requests.length + " nouvelles requêtes effectuées :</b><br>";
		    }
		    for (var i = 0; i < requests.length; i++){
			message = message + "Envoyée le : " + JSON.stringify(requests[i]["called_at"]) + ", au serveur : " + JSON.stringify(requests[i]["server"]) + ", sujet : " + JSON.stringify(requests[i]["query"]) + "<br>";
		    }
		} 
	        transport.sendMail({from: 'projettwitter22@gmail.com', to, subject, html: message}, function(error, info){
                    if (error) {
                        console.log(error);
                    }
                    else {
                        console.log(`Message sent: ${info.response}`);
                        dailyLogReset();
			message = "";
                    };
		})
	})
	.catch(err => { console.log(err) });
    });
};
