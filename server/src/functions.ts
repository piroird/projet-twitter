import { callTwitter, getSearchQuery } from "./twitter.js";
import { Position } from "./models/Position.js";
import { Request_ } from "./models/Request_.js";
import { saveRequest } from "./redis.js";

function getPositions(data: any) : Position[]{
  return data.includes?.places?.map((place: any) => {
    let centerLong: number = (place.geo.bbox[0] + place.geo.bbox[2]) / 2; // Longitude
    let centerLat: number  = (place.geo.bbox[1] + place.geo.bbox[3]) / 2; // Latitude

    let tweet_id: string = data.data
      .find((el: any) => el.geo?.place_id == place.id)
      .id;

    return new Position(
      tweet_id,
      place.country_code,
      Number(centerLong.toFixed(7)),
      Number(centerLat.toFixed(7))
    );
  }) ?? [];
}

export function getLocations(query: string, limit=10, minimum=5) : Promise<Position[]>{
  return new Promise(async (resolve, reject) => {
    let positions: Position[] = []
    let next_token: string|null = null;
    let cpt: number = 0;

    try{
      do {
        let url = getSearchQuery(query, next_token);
        let res = await callTwitter(url);

        next_token = res.meta.next_token;
        let newPositions = getPositions(res);
        positions.push(...newPositions);

      } while(positions.length < minimum && cpt++ < limit);

      //log this request to cache
      saveRequest(new Request_(query, "Twitter", cpt)).then().catch((e) => reject(e));
      
      resolve(positions);
    }
    catch (e){
      reject(e);
    }

  });
}
