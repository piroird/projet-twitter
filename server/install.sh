npm install

fillEnvFile(){
  MSG=$1
  ENV_VAR_NAME=$2

  echo ">> $MSG :"
  read v
  echo "$ENV_VAR_NAME=$v" >> $FILE
}

FILE=".env"
if [[ -f $FILE ]];then
    echo ">> $FILE exists"
else
  fillEnvFile "Enter Twitter Bearer token" "BEARER"
  fillEnvFile "Enter express server port" "PORT"
fi
