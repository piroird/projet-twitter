# Projet Twitter
## Description
Projet de fin d'étude à 4. L'objectif de l'application web est de créer une carte
des localisations des différents tweets comportant un certain mot-clé.


## Technologie
- Node.js
  - Express
  - Typescript
- Docker
- Angular
  - Leaflet


## Installation
- Récupérez le bearer d'un compte développeur Twitter
- Appelez le script `install.sh`
  - Remplissez les champs demandés


## Installation avec docker
- Remplissez le fichier .env en prenant comme modèle .env.example
- Appeler la commande `docker build -t projet-twitter .` pour créer l'image.
- Lancer le conteneur avec `docker run -d -p 8000:8000 projet-twitter`.


## Commandes
| Commande | Description |
|----------|-------------|
| `npm run build` | Démarrage de la compilation |
| `npm run start` | Lancement de l'application |
| `npm run dev`   | Compilation et lancement de l'application |
| `npm run clear` | Suppression les fichiers compilés |


## API
- `/getLocations/` : renvoie la liste des positions
  - query : la recherche envoyée à Twitter
  - minimum (optionnel, defaut 5) : le nombre de position minimum à renvoyer
  - limit (optionnel, defaut 10) : le nombre maximum de page recherchée
