import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as L from 'leaflet';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MarkerService {
  private tweets: string = environment.apiUrl.endsWith("/") ?
      `${environment.apiUrl}getLocations` : `${environment.apiUrl}/getLocations`;

  constructor(private http: HttpClient) { }

  private getPopUpFromTweetID(id: number): string{
    return `<a href="https://twitter.com/anyuser/status/${id}"
            target="_blank">Tweet disponible ici</a>`;
  }

  makeTweetMarkers( map: L.Map, query: string): Promise<void> {
    let url = `${this.tweets}?query=${encodeURIComponent(query)}`;

    return new Promise(resolve => {
      this.http.get<any>(url).subscribe((res: any) => {
        for (const location of res) {
          const lon = location.longitude;
          const lat = location.latitude;
          const marker = L.marker([lat, lon]);

          marker.bindPopup(this.getPopUpFromTweetID(location.tweet_id));
          marker.addTo(map);
        }
        resolve();
      });
    });
  }
}
