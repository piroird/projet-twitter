import { Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector:'app-navbar',
    templateUrl:'./navbar.component.html',
    styleUrls:['./navbar.component.css']
    })

export class NavBarComponent{
   @Output() search: EventEmitter<string> = new EventEmitter();
   query: string = "";

   sendSearch(): void {
     this.search.emit(this.query);
   }
}
