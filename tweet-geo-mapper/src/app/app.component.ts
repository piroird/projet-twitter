import { Component } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tweet-geo-mapper';
  eventsSubject: Subject<string> = new Subject<string>();

  sendSearch(query: string): void {
    this.eventsSubject.next(query);
  }
}
