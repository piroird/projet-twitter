import { Component, AfterViewInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import * as L from 'leaflet';
import { MarkerService } from '../marker.service';

const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit {
  @Input() searchObservable: Observable<string>|undefined;
  private map: L.Map|undefined;
  showLoader: boolean = false;

  constructor(private markerService: MarkerService) { }

  private initMap(): void {
    this.map = L.map('map', {
      center: [ 39.8282, -98.5795 ],
      zoom: 3
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
  }

  private removeMarkers(): void {
    this.map!.eachLayer((layer) => {
      if("_url" in layer){}
      else{this.map!.removeLayer(layer);}
    });
  }

  ngAfterViewInit(): void {
    this.initMap();

    this.searchObservable!.subscribe((query: string) => {
      this.showLoader = true;
      this.removeMarkers();

      this.markerService.makeTweetMarkers(this.map!, query)
        .then(() => this.showLoader = false);
    });
  }
}
